# Jupyter Notebooks for Training Digital Humanists

This growing collection of Jupyter Notebooks has been used for training students in the Digital Studio internship program at the University of Melbourne.
